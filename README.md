# OkadaLab. Docker Images
This repo contains Dockerfiles for use in automated builds on https://hub.docker.com/r/autonomoustuff/docker-builds/. It contains at least the following (listed by image tag):

docker pull ros:noetic-ros-core-focal

docker pull autonomoustuff/docker-builds:noetic-perception


## Ubuntu Images
okdhryk/ubuntu:focal
okdhryk/ubuntu:focal-nvidia
okdhryk/ubuntu:jammy
okdhryk/ubuntu:jammy-nvidia



## ROS Images
okdhryk/noetic:desktop-full
okdhryk/noetic:desktop-full-nvidia



noetic-ros-base
noetic-robot
noetic-perception
noetic-perception-opencv-cuda


## ROS2 Images